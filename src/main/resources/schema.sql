create table persona (id bigint identity primary key, nombre varchar(10), edad int);

insert into persona (nombre , edad)
values('Juan',20),
('Jhonny', 20),
('Fran', 20),
('coco', 40),
('Leo', 30),
('Mati', 30),
('Lol', 10),
('rushero', 10);
