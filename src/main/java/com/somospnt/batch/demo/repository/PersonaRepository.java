package com.somospnt.batch.demo.repository;

import com.somospnt.batch.demo.domain.Persona;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PersonaRepository extends PagingAndSortingRepository<Persona, Long>{
    
}
