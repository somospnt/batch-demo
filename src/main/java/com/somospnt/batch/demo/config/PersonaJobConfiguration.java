package com.somospnt.batch.demo.config;

import com.somospnt.batch.demo.repository.PersonaRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class PersonaJobConfiguration {

    @Autowired
    private PersonaRepository personaRepository;

    @Bean
    public ItemReader personaJpaReader() {
        return null;
    }

    @Bean
    public ItemWriter personaFileWriter() {
        return null;
    }

    @Bean
    public Step stepPersona(StepBuilderFactory stepBuilders) {
        return null;
    }

    @Bean
    public Job personaJob(JobBuilderFactory jobBuilders, Step stepPersona) {
        return jobBuilders.get("personaJob").start(stepPersona).build();
    }
    
}
